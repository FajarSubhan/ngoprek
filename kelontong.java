/*
* Created By Fajar Subhan
*
* Created By 20221119
* */

import java.util.Scanner;

class kelontong {
    public void run()
    {
        /* Deklarasikan variable / property
        * Data barang : kode_barang,nama_barang,harga_barang,Bonus
        * Total       : total_harga_barang,jumlah_barang
        *
        * ada 2 property yang tidak disebut didalam soal nomer 1 yaitu
        * 1.Total harga barang  = Total harga dari setiap pembelian
        * 2.Bonus               = untuk menampung string bonus
        * */

        String nama_barang,bonus = null;
        int kode_barang,harga_barang,jumlah_barang,total_harga_barang;

        // Buat object untuk menerima masukan data dari user
        Scanner input = new Scanner(System.in);

        System.out.println("============== Selamat datang di toko Kelontong mart ===============");
        System.out.print("------------------------------------");
        System.out.println();

        System.out.println("Kode     Nama               Harga");
        System.out.println("101   Gula Pasir            10000");
        System.out.println("201   Indomie  Goreng       3500");
        System.out.println("301   Minyak Goreng         14000");
        System.out.println("401   Beras                 85000");
        System.out.println("------------------------------------");

        /* 1.Input Data */
        System.out.println("Masukan kode barang : ");
        kode_barang = input.nextInt();

        System.out.println("Masukan jumlah barang : ");
        jumlah_barang = input.nextInt();

        /* 2.Proses */
        // Membuat kondisi if majemuk untuk menentukan
        // dan menampilkan nama barang yang akan dipilih dari user
        if(kode_barang == 101)
        {
            nama_barang         = "Gula Pasir";
            harga_barang        = 100000;
            total_harga_barang  = harga_barang * jumlah_barang;
        }
        else if(kode_barang == 201)
        {
            nama_barang  = "Indomie Goreng";
            harga_barang = 3500;
            total_harga_barang  = harga_barang * jumlah_barang;

        }
        else if(kode_barang == 301)
        {
            nama_barang  = "Minyak Goreng";
            harga_barang = 14000;
            total_harga_barang  = harga_barang * jumlah_barang;

        }
        else if(kode_barang ==  401)
        {
            nama_barang  = "Beras";
            harga_barang = 85000;
            total_harga_barang  = harga_barang * jumlah_barang;
        }
        else
        {
            nama_barang         = "Nama barang tidak ditemukan";
            harga_barang        = 0;
            total_harga_barang  = 0;
        }

        /* Menentukan bonus yang didapatkan */
        if(total_harga_barang < 100000)
        {
            bonus = "Gratis 2 Saschet kopi";
        }
        else if(total_harga_barang >= 100000)
        {
            bonus = "Gratis 5 Bungkus Indomie";
        }
        else if(total_harga_barang >= 250000)
        {
            bonus = "Gratis 1 Liter Minyak Goreng";
        }

        /* 3.Output */
        System.out.println("Daftar Pembelian");
        System.out.println("-------------------------");

        System.out.print("Kode Barang : " + kode_barang);
        System.out.print("Nama Barang : " + nama_barang);
        System.out.print("Harga Satuan: " + harga_barang);

        System.out.println();
        System.out.println();

        System.out.println("Qty     : " + jumlah_barang);
        System.out.println("Total   : Rp." + total_harga_barang);

        System.out.println();

        System.out.println("Bonus : Selamat anda mendapatkan " + bonus);

    }
}

class main {
    public static void main(String[] data)
    {
        kelontong obj = new kelontong();
        obj.run();
    }
}